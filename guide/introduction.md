---
label: Introduction
order: 1000
---

# Mangadex API Guide

## Introduction

Here begin guides for several common uses for the Mangadex API. We will try to cover most of the features already found in our website, while making you aware of how to customize your requests for your specific goals.

!!!info
The guides are still incomplete, and we're more than welcome to people [contributing to them](https://gitlab.com/mangadex-pub/mangadex-api-docs). If you have questions about how to contribute, reach out to us in the #dev-talk-api channel in our [discord](https://discord.gg/mangadex) server.
!!!

The guides will initially show code examples in Python and JavaScript. For Python, we'll use the [requests](https://pypi.org/project/requests/) module. For JavaScript, the [axios](https://www.npmjs.com/package/axios) package will allow the tutorials to be much simpler while focusing on the content rather than the functionality. The code examples will be made with the target of allowing you to copy-paste it into your editor or REPL easily.

Our API has full support for editing pages and several resource metadata. Keeping things in order is a heavy enough load for the site moderators and contributors. Therefore, if you're unsure of whether your program won't break and inadvertently make changes to thousands of manga due to, say, a recursion issue, **do not make any such attempts to edit pages.** Make sure your program is bug-free by testing it in a local test environment where you present yourself the before and after your program's functionality. Any conduct that distorts the site's data en masse will be followed by a revocation of editing permissions.

## Contribution

The guides are new, hence there's much room for adding new guides, expanding upon the existing ones, editing errors, or pending updates. We welcome you to contribute to these guides in our [Gitlab repo](https://gitlab.com/mangadex-pub/mangadex-api-docs). To maintain consistency, there are of course certain guidelines that you'll have to follow, depending on how you wish to contribute. If the way you want to contribute is not covered by this document, please consult with us on our official [discord](https://discord.gg/mangadex) beforehand.

### Adding more languages to the code examples

The guides will always have Python and JavaScript code examples to cover the largest percentage of consumers, but we always welcome people adding more examples in a language they're confident in. Before submitting a merge request, please make sure that

- Your code is copy-pastable on the terminal (for example, an empty line before the indented block is expected to close will throw an error in Python's REPL).
- Your code is clean, straightforward, and simple. Remember that this is not to show off your skills as a programmer, but help new programmers who might not be the most well-versed in that language. If comprehensibility comes at the cost of efficiency, then so it should be. This is not to discourage incorporating efficient solutions or best practices, but to state that simplicity should be a higher priority.
- You have covered a meaningful amount of the guide sections. There's no use to adding a code example for only one section of the guides. It's not mandatory to cover the guides in their entirety, but make sure that your contribution will actually be helpful for those who want to consult on it.

All the code examples must be wrapped around a `:::code-block:::` component on the markdown. Your markdown code should look something like this:
````
:::code-block
```python
print("Hello, World!")
```
:::
````

This makes the code block have a fixed max-height as per the custom CSS, and be scrollable on the inside.

!!!Tip
To provide an interactive example, you might need to set variables/constants like a specific Manga's ID. Try to keep them on separate code blocks from the parts where the request is sent and handled. This will help users copy paste the logic while setting those parameters on their own if they wish to test them on the resources of their choice.
!!!

Try to make the code examples useful towards each other. For example, it would be nice if the user can use the code from the Login example to proceed with using an authenticated resource without wasting time on figuring it out or writing it on their own. This is achieved simply by using the same variable names across the examples, like `sessionToken` for example.
