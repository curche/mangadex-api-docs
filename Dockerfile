FROM registry.gitlab.com/mangadex-pub/nginx-mainline:bullseye

ENV NGINX_CONF_FILE /opt/mangadex/nginx.conf
ENV NGINX_TMP_DIR   /tmp/nginx-tmp

COPY --chown=root:root docker/nginx.conf /opt/mangadex/nginx.conf
COPY --chown=root:root ./.retype         /var/www/mangadex/docs
COPY --chown=root:root ./static/api.yaml /var/www/mangadex/docs

USER mangadex
WORKDIR /tmp
